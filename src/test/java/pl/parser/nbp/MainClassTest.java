package pl.parser.nbp;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by JMigas on 2017-08-12.
 */
public class MainClassTest {


    @DataProvider(name = "correctInput")
    public static Object[][] correctInput() {

        return new Object[][]{
                {new String[]{"USD", "2017-08-03", "2017-08-03"}},
                {new String[]{"USD", "2017-01-02", "2017-01-23"}},
                {new String[]{"USD", "2017-01-02", "2017-04-23"}},
        };
    }

    @Test(dataProvider = "correctInput")
    public void shouldPass(String[] args) throws Exception {
        MainClass.main(args);
    }

    @DataProvider(name = "faultyInput")
    public static Object[][] faultyInput() {

        return new Object[][]{
//                {null},
//                {new String[]{}},
//                {new String[]{"pfff"}},
//                {new String[]{"pfff", "1", "2"}},
//                {new String[]{"USD", "1"}},
//                {new String[]{"USD", "2017-01-01", "2"}},
//                {new String[]{"pffs", "2017-01-01", "2017-01-15"}},
//                {new String[]{"pffs", "1", "2017-01-15"}},
//                {new String[]{"pffs", "as", "as"}},
//                {new String[]{"USD", "as", "as"}},
//                {new String[]{"USD", "2017-06-01", "2017-01-15"}},

                {new String[]{"EUR", "1908-08-03", "2017-08-03"}},
                {new String[]{"USD", "1908-08-03", "2017-10-03"}},
                {new String[]{"EUR", "1999-01-01", "1999-01-15"}},
                {new String[]{"USD", "2017-09-01", "2017-10-15"}},
                {new String[]{"USD", "2018-01-01", "1999-01-15"}},
        };
    }

    @Test(dataProvider = "faultyInput", expectedExceptions = Exception.class)
    public void shouldFail(String[] args) throws Exception {
        MainClass.main(args);
    }

}