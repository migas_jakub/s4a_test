package pl.parser.nbp;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.stream.Stream;

import static org.apache.commons.lang3.math.NumberUtils.toInt;

/**
 * Created by JMigas on 2017-08-11.
 */
public class NBPQueryParams {

    public enum TYPE {
        USD, AUD, CAD, EUR, HUF, CHF, GBP, JPY, CZK, DKK, NOK, SEK, XDR
    }

    private TYPE type;
    private int buyDate;
    private int sellDate;

    private NBPQueryParams() {
    }

    public static NBPQueryParams createNBPQuery() {
        return new NBPQueryParams();
    }

    public NBPQueryParams fromInputData(String[] args) throws Exception {
        validateInput(args);
        return this;
    }

    private void validateInput(String[] args) throws Exception {
        try {
            if (args != null && args.length == 3) {
                type = validateCurrencyAndGet(args[0]);
                buyDate = toInt(getDateOf(args[1]));
                sellDate = toInt(getDateOf(args[2]));
                if (sellDate < buyDate)
                    throw new IllegalArgumentException("Wrong time bounding. Sell date must be later than buy date.");
            } else {
                throw new IllegalArgumentException("Insufficient arguments. Must be (3): Currency_abbrev but_date sell_date");
            }
        } catch (DateTimeException e) {
            throw new Exception("Invalid date. Format is: YYYY-MM-dd", e);
        }
    }

    private String getDateOf(String arg) throws DateTimeException {
        return LocalDate.parse(arg).format(DateTimeFormatter.ofPattern("YYMMdd"));
    }

    private TYPE validateCurrencyAndGet(String arg) {
        Optional<TYPE> currency = Stream.of(TYPE.values()).filter(typ -> typ.toString().equalsIgnoreCase(arg.toString())).findFirst();
        if (arg.length() != 3 && !currency.isPresent())
            throw new IllegalArgumentException("Illegal argument {Currency_abbrev}. Must be one of the known name, like USD, EUR");
        else
            return currency.get();
    }

    public TYPE getType() {
        return type;
    }

    public int getBuyDate() {
        return buyDate;
    }

    public int getSellDate() {
        return sellDate;
    }
}
