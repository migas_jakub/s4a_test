package pl.parser.nbp;

import com.thoughtworks.xstream.converters.SingleValueConverter;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * Created by JMigas on 2017-08-10.
 */
public class LocalNumberConverter implements SingleValueConverter {
    private NumberFormat format = NumberFormat.getInstance(Locale.GERMAN);

    public boolean canConvert(Class clazz) {
        return clazz.equals(Double.class);
    }

    public String toString(Object o) {
        return format.format(o);
    }

    public Object fromString(String str) {
        try {
            return format.parse(str).doubleValue();
        } catch (ParseException ex) {
            throw new IllegalArgumentException(str, ex);
        }
    }
}
