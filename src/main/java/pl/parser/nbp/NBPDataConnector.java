package pl.parser.nbp;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang3.math.NumberUtils.toInt;
import static pl.parser.nbp.MainClass.log;

/**
 * Created by JMigas on 2017-08-10.
 */
public class NBPDataConnector {

    private static final String date_bound_err_msg = "Cannot find data on server for specified date range";

    public static final String FILE_PATTERN = "%sz%d.xml";
    public static final String PATH = "http://www.nbp.pl/kursy/xml/";
    public static final String DIR_FILE = "dir.txt";
    private String encoding = "ISO-8859-2";

    private String listEncoding = "UTF-8";

    List<NBPSnapshot> snapshots = Collections.synchronizedList(new ArrayList<>());

    public void callForDateFrame(NBPQueryParams params) throws Exception {
        try {
            List<String> dirList = callNBPList();
            if (dateOutOfBounds(params, dirList)) {
                log(date_bound_err_msg);
                throw new Exception(date_bound_err_msg);
            }

            dirList.parallelStream()
                    .map(e -> e.split("z"))
                    .filter(e -> e[0].startsWith("c") && isInDateFrame(params, e[1]))
                    .forEach(entry -> callToNBPServer(entry[0], toInt(entry[1])));

        } catch (IOException | URISyntaxException e) {
            log("%nCall failed, %s", e.getMessage());
        }
    }

    private boolean dateOutOfBounds(NBPQueryParams params, List<String> dirList) {
        return dirList.isEmpty() ||
                toInt(dirList.get(0).split("z")[1]) > params.getBuyDate() ||
                toInt(dirList.get(dirList.size() - 1).split("z")[1]) < params.getSellDate();
    }

    public List<NBPSnapshot> getSnapshots() {
        return snapshots;
    }


    private List<String> callNBPList() throws IOException, URISyntaxException {
        List<String> lines = IOUtils.readLines(new URL(PATH + DIR_FILE).openStream(), listEncoding);
        return lines;
    }

    private void callToNBPServer(String head, int date) {
        try {
            List<String> file = connectToNBP(String.format(PATH + FILE_PATTERN, head, date));
            snapshots.add(NBPXmlParser.collectAndDisplay(file.stream().reduce(String::concat).get()));
        } catch (IOException | URISyntaxException e) {
            log("ERR.NBP.SERVER_CALL {Error while calling NBP server for data}");
            e.printStackTrace();
        }
    }

    private List<String> connectToNBP(String resource) throws IOException, URISyntaxException {
        List<String> lines = IOUtils.readLines(new URL(resource).openStream(), encoding);
        return lines;
    }


    private boolean isInDateFrame(NBPQueryParams params, String value) {
        int date = toInt(value);
        return date >= params.getBuyDate() && date <= params.getSellDate();
    }
}
