package pl.parser.nbp;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by JMigas on 2017-08-10.
 */
@XStreamAlias("tabela_kursow")
public class NBPSnapshot {

    private String typ = "C";
    private String uid;

    @XStreamAlias("numer_tabeli")
    private String nrTabeli;
    @XStreamAlias("data_notowania")
    private String dataNotowania;
    @XStreamAlias("data_publikacji")
    private String dataPublikacji;

    @XStreamImplicit(itemFieldName = "pozycja")
    private List<Pozycja> pozycje = new ArrayList();

    public String getTyp() {
        return typ;
    }

    public String getUid() {
        return uid;
    }

    public String getNrTabeli() {
        return nrTabeli;
    }

    public String getDataNotowania() {
        return dataNotowania;
    }

    public String getDataPublikacji() {
        return dataPublikacji;
    }

    public List<Pozycja> getPozycje() {
        return pozycje;
    }

    public class Pozycja {

        @XStreamAlias("nazwa_waluty")
        private String nazwaWaluty;
        @XStreamAlias("przelicznik")
        private int przelicznik;
        @XStreamAlias("kod_waluty")
        private String kodWaluty;
        @XStreamAlias("kurs_kupna")
        private Double kursKupna;
        @XStreamAlias("kurs_sprzedazy")
        private Double kursSprzedazy;

        public Pozycja() {
        }

        public String getNazwaWaluty() {
            return nazwaWaluty;
        }

        public int getPrzelicznik() {
            return przelicznik;
        }

        public String getKodWaluty() {
            return kodWaluty;
        }

        public Double getKursKupna() {
            return kursKupna;
        }

        public Double getKursSprzedazy() {
            return kursSprzedazy;
        }
    }
}
