package pl.parser.nbp;

/**
 * Created by JMigas on 2017-08-11.
 */
public class Calculation {
    private double avg;
    private double dev;

    private Calculation() {
    }

    public static Calculation create() {
        return new Calculation();
    }

    public double getAvg() {
        return avg;
    }

    public Calculation withAvg(double avg) {
        this.avg = avg;
        return this;
    }

    public double getDev() {
        return dev;
    }

    public Calculation withDev(double dev) {
        this.dev = dev;
        return this;
    }
}
