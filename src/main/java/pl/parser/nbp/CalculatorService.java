package pl.parser.nbp;

import pl.parser.nbp.NBPQueryParams.TYPE;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by JMigas on 2017-08-11.
 */
public class CalculatorService {

    private NBPDataConnector connector = new NBPDataConnector();

    public Calculation evaluate(NBPQueryParams params) throws Exception {
        connector.callForDateFrame(params);
        List<NBPSnapshot> list = connector.getSnapshots();

        Double avg = 0D;
        Double stdDev = 0D;

        if (!list.isEmpty()) {
            avg = Calculator.callAvg(getAvgData(params.getType(), list));
            stdDev = Calculator.callDeviation(getDevData(params.getType(), list));
        }

        return Calculation.create()
                .withAvg(avg)
                .withDev(stdDev);
    }

    private List<Double> getAvgData(TYPE type, List<NBPSnapshot> list) {
        return list.stream()
                .map(e -> e.getPozycje().stream()
                        .filter(p -> p.getKodWaluty().equalsIgnoreCase(type.name()))
                        .map(NBPSnapshot.Pozycja::getKursKupna)
                        .collect(Collectors.toList())
                )
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    private List<Double> getDevData(TYPE type, List<NBPSnapshot> list) {
        return list.stream()
                .map(e -> e.getPozycje().stream()
                        .filter(p -> p.getKodWaluty().equalsIgnoreCase(type.name()))
                        .map(NBPSnapshot.Pozycja::getKursSprzedazy)
                        .collect(Collectors.toList())
                )
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }
}
