package pl.parser.nbp;

import static pl.parser.nbp.NBPQueryParams.createNBPQuery;

/**
 * Created by JMigas on 2017-08-10.
 */
public class MainClass {

    private static CalculatorService service = new CalculatorService();

    public static void main(String[] args) throws Exception {

        Calculation results = service.evaluate(createNBPQuery().fromInputData(args));

        log("%nAvg: %.4f%nStd Dev: %.4f", results.getAvg(), results.getDev());
    }

    public static void log(Object o) {
        System.out.println(o);
    }

    public static void log(String rgx, Object... o) {
        System.out.printf(rgx, o);
    }
}
