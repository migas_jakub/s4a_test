package pl.parser.nbp;

import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import java.util.List;

/**
 * Created by JMigas on 2017-08-10.
 */
public class Calculator {

    public static Double callAvg(List<Double> data) {
        return data.stream()
                .mapToDouble(Double::doubleValue)
                .average()
                .getAsDouble();
    }

    public static Double callDeviation(List<Double> data) {
        StandardDeviation dev = new StandardDeviation();

        double[] doubles = data.stream()
                .mapToDouble(Double::doubleValue)
                .toArray();

        return dev.evaluate(doubles);
    }

}
