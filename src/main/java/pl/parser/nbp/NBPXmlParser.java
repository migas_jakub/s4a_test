package pl.parser.nbp;


import com.thoughtworks.xstream.XStream;

import java.io.IOException;

/**
 * Created by JMigas on 2017-08-10.
 */
public class NBPXmlParser {

    public static NBPSnapshot collectAndDisplay(String file) throws IOException {
        XStream xstream = new XStream();
        xstream.registerConverter(new LocalNumberConverter());
        xstream.processAnnotations(NBPSnapshot.class);
        xstream.processAnnotations(NBPSnapshot.Pozycja.class);
        NBPSnapshot value = (NBPSnapshot) xstream.fromXML(file);
        return value;
    }
}
